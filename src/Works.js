
import HelloWorld from './components/HelloWorld.js'

export default {
  name: 'App',
  components: {
    HelloWorld
  }
}
